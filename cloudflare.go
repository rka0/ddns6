package main

import (
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"log"
	"strings"
)

// init api
func initCfApi(keyArg string) cloudflare.API {
	//fmt.Println("initCfApi")
	api, err := cloudflare.NewWithAPIToken(keyArg)
	if err != nil {
		log.Fatal(err)
	}
	return *api
}

// create record at cloudflare
func (h *Host) createDNSRecord(zoneArg string, recordArg cloudflare.DNSRecord) {
	//fmt.Println("createDNSRecord")
	// make the request
	apiResp, err := h.api.CreateDNSRecord(zoneArg, recordArg)
	if err != nil {
		log.Fatal(err)
	}
	// update record id
	h.record.ID = apiResp.Result.ID
}

// checks if host record exists in zone
func (h *Host) checkForHostRecord(zoneArg string) (recordExists bool) {
	//fmt.Println("checkForHostRecord")
	recEx := false

	// make the request
	apiResp, _ := h.api.DNSRecords(zoneArg, cloudflare.DNSRecord{})

	// iterate through records in zone
	for _, r := range apiResp {
		// convert to lowercase on this because :cloudflare:
		// apparently they convert CreateDNSRecord to lower on the api
		if r.Name == strings.ToLower(fmt.Sprintf("%s.%s", h.Hostname, recordSubdomain)) {
			// update that record was found, and set ID + record that already exists
			recEx = true
			h.record.ID = r.ID
			h.record.Content = r.Content
		}
	}
	//fmt.Println(recEx)
	return recEx
}

// updates host record in zone with DNSRecord
func (h *Host) updateHostRecord(zoneArg string, recordIdArg string, recordArg cloudflare.DNSRecord) {
	//fmt.Println("updateHostRecord")
	// make the request
	err := h.api.UpdateDNSRecord(zoneArg, recordIdArg, recordArg)
	if err != nil {
		log.Fatal(err)
	}
}
