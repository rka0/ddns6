package main

import (
	"fmt"
	"github.com/cloudflare/cloudflare-go"
	"os"
	"time"
)

const (
	apiKey = "" // api key
	zoneId = "" // get domain id manually
	recordSubdomain = "" // "dyndns.mydomain.com" will result in "client1.dyndns.mydomain.com"
)

// initial stuff for startup
func (h *Host) Startup() {
	// need ZoneId beforehand, otherwise this would need elevated api permissions at cloudflare
	h.ZoneId = zoneId
	h.Hostname, _ = os.Hostname()

	// init api object
	h.api = initCfApi(apiKey)

	// init self record object defaults
	h.record = cloudflare.DNSRecord{
		Type:       "AAAA",
		Name:       fmt.Sprintf("%s.%s", h.Hostname, recordSubdomain),
		Proxied:    false,
		ZoneID:     h.ZoneId,
	}

	if h.checkForHostRecord(h.ZoneId) { // if record exists at api.. (this also sets h.record.ID if it does exist)
		fmt.Printf("[ddns6] record for %s exists in zone %s\n", h.Hostname, recordSubdomain)
		if h.updateHostCheck() { // and if update is needed vs whats reflected in api..
			fmt.Printf("[ddns6] updating address: [%s] -> ", h.record.Content)
			// set new ip locally, then trigger api update
			h.record.Content = getDefaultIpv6()
			h.updateHostRecord(h.ZoneId, h.record.ID, h.record)
			fmt.Printf("[%s]\n", h.record.Content)
		} else {
			fmt.Println("[ddns6] record is up to date")
		}
	} else { // if record doesn't exist at api..
		fmt.Printf("[ddns6] creating new record for %s\n", h.Hostname)
		// set new ip locally, then trigger api update
		h.record.Content = getDefaultIpv6()
		h.createDNSRecord(h.ZoneId, h.record)
	}
}

// background loop
func (h *Host) DaemonLoop() {
	for {
		if h.updateHostCheck() { // if the preferred ip has changed..
			// set new ip locally, then trigger update
			fmt.Printf("[ddns6] preferred ipv6 address changed to: %s\n", h.record.Content)
			h.record.Content = getDefaultIpv6()
			h.updateHostRecord(h.ZoneId, h.record.ID, h.record)
		}
		time.Sleep(10 * time.Second) // update delay
	}
}

func init() {
	fmt.Println("*** ddns6 dynamic dns client starting..")
}

func main() {
	// create self host object
	self := new(Host)

	// do initial stuff
	self.Startup()

	// background loop
	self.DaemonLoop()
}
