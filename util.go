package main

import (
	"log"
	"net"
)

// get default/preferred ipv6 as string
func getDefaultIpv6() (externalAddr string) {
	// set dialer to use a v6 address, using udp since stateless
	conn, err := net.Dial("udp6", "[2620:fe::9]:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr).IP.String()
	return localAddr
}

// checks if address has changed vs what is in host object, returns bool
func (h *Host) updateHostCheck() (updateNeeded bool) {
	//fmt.Println("updateHostCheck")
	updateBool := false
	addrsMatch := getDefaultIpv6() == h.record.Content
	if addrsMatch {
		// address has not changed, do nothing..
	} else {
		// address has changed
		updateBool = true
	}
	//fmt.Println("updateBool:", updateBool)
	return updateBool
}
