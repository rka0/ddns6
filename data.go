package main

import (
	"github.com/cloudflare/cloudflare-go"
)

// some self data
type Host struct {
	Hostname string
	ZoneId	 string
	api      cloudflare.API
	record   cloudflare.DNSRecord
}
